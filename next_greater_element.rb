
find_nums = [4, 1, 2]
nums = [1, 3, 4, 2]

def next_greater_element(find_nums, nums)
  result = []
  nums_hash = {}

  nums.each_with_index do |num, i|
    nums_hash[num] = i
  end

  find_nums.each do |num|
    unless nums_hash[num]
      result << -1
      next
    end

    i = nums_hash[num]
    i += 1 while i < nums.length && nums[i] <= num
    result << if i < nums.length
                nums[i]
              else
                -1
              end
  end

  p result
end

next_greater_element(find_nums, nums)
