prices = [7,1,5,3,6,4]
prices_2 = [1,2,3,4,5]
prices_3 = [7,6,4,3,1]

def max_profit(prices)
  profit = 0

  prices.each_with_index do |price, i|
    if prices[i + 1] && price < prices[i + 1]
      profit += (prices[i + 1] - price)
    end
  end
  p profit
end

max_profit(prices)
