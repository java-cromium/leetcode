# @param {Integer[]} nums
# @param {Integer} target
# @return {Integer[]}
nums = [9, 8, 8, 6, 8, 3, 9, 11]

#code below returns the index of the two numbers that add up to the target
def two_sums(nums, target)
  dict = {}
  nums.each_with_index do |n, i|
      return [dict[target - n], i] if dict[target - n]
    dict[n] = i
  end
end
p two_sums(nums, 14)

#code below prints the two numbers that add up to the target
def two_sum_print_nums(nums, target)
  hash = {}
  nums.each_with_index do |number, index|
    if hash.value?(target - number)
      result = [hash[hash.key(target - number)], number]
      return result
    end
    hash[index] = number
  end
end

p two_sum_print_nums(nums,14)

#this is the same as the method that starts on line #7 but more explicitly written
def two_sum(nums, target)
  dict = {}
  nums.each_with_index do |n, i|
    if dict[target - n]
      return dict[target - n], i
    end
    dict[n] = i
  end
end

p two_sum(nums, 14)

# # This is the logic used to save the contents of an array as a hash,
# # using the original array's value as a key in the hash and the array's index as a value in the hash.
# dict = {}
#  nums.each_with_index do |n, i|
#    dict[n] = i
#  end
#