# frozen_string_literal: true

arr = ["5","2","C","D","+"]

def cal_points(ops)
  a = []
  for i in 0..ops.size-1
    if !!ops[i].match(/[0-9]/)
      a.push(ops[i].to_i)
    elsif ops[i] == "C"
      a.pop()
    elsif ops[i] == "D"
      a.push(a.last*2)
    else
      a.push(a.last(2).sum)
    end
  end
  return a.reduce(:+)
end

p cal_points(arr)