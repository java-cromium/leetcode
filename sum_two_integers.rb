# frozen_string_literal: true

a = 214
b = -21

#
# def get_sum(a, b)
#   count = 0
#   a_array = (0..a).to_a
#   b_array = (0..b).to_a
#   a_array = (a..0).to_a if a_array.length == 0
#   b_array = (b..0).to_a if b_array.length == 0
#
#   a_array.each do |elem|
#     if elem > 0
#       count += 1
#     elsif elem < 0
#       count -= 1
#     end
#   end
#   b_array.each do |num|
#     if num > 0
#       count += 1
#     elsif num < 0
#       count -= 1
#     end
#   end
#   count
# end

def get_sum(a, b)

  # http://stackoverflow.com/questions/8698959/how-to-force-ruby-to-store-a-small-number-as-32-bit-integer
  a = Array(a).pack('l').unpack('l').first
  b = Array(b).pack('l').unpack('l').first

  # https://discuss.leetcode.com/topic/50178/golang-0ms-with-explantation
  #   A	B	C	S
  #   0	0	0	0
  #   1	0	0	1
  #   0	1	0	1
  #   1	1	1	0

  return a if b.zero?
  return b if a.zero?

  sum   = a ^ b
  carry = (a & b) << 1

  get_sum(sum, carry)
end



p get_sum(a, b)
