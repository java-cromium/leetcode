# frozen_string_literal: true

x = 680_142_203
y = 1_111_953_568

def hamming_distance(x, y)
  result = 0
  x_array = x.to_s(2)
  y_array = y.to_s(2)
  x_array.prepend('0') while x_array.length < 31
  y_array.prepend('0') while y_array.length < 31
  i = 0
  while i < x_array.length
    result += 1 unless x_array[i] == y_array[i]
    i += 1
  end
  p result
end

hamming_distance(x, y)
