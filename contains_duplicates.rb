array1 = [4, 5, 19, 6, 7, 19, 8, 9]

def contains_duplicate(nums)
  dict = {}
  nums.each_with_index do |number, index|
    dict[number] = index
  end
  dict.length < nums.length ? true : false
end

p contains_duplicate(array1)