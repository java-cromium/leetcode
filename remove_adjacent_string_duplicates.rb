s = "abbbbaaaca"

def remove_dups(string)
  chars = []
  string.each_char do |char|
    if chars[-1] == char
      chars.pop
    else
      chars << char
    end
  end
  chars.join
end

remove_dups(s)

