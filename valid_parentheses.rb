s1 = "([)]" # output false
s2 = "{[]}" # output true
s3 = "()[]{}" # output true

def is_valid(s)
  return true if s.empty?

  stack = []
  s.each_char do |elem|
    case elem
    when '(', '{', '[' then stack.push(elem)
    when ')' then return false if stack.pop != '('
    when '}' then return false if stack.pop != '{'
    when ']' then return false if stack.pop != '['
    end
  end
  stack.empty?
end

p is_valid(s1)
p is_valid(s2)
p is_valid(s3)



