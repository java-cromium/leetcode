a = [2, 7, 5, 4, 7, 8, 12, 13]

def sort_array_by_parity(a)
  result = []
  a.each { |num| num % 2 == 0 ? result.unshift(num) : result.push(num)}
  p result
end

sort_array_by_parity(a)
