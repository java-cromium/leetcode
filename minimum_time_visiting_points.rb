points = [[1,1],[3,4],[-2,0]]


def min_time_to_visit_all_points(points)
  seconds = 0
  points.each_with_index do |point, i|
    break if points[i+1].nil?
    seconds += [(point[0] - points[i+1][0]).abs, (point[1] - points[i+1][1]).abs].max
  end
  p seconds
end

# p [[1,3],[5,6],[8,9]].to_h

min_time_to_visit_all_points(points)
