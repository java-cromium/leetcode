# Given an integer number n, return the difference between the product of its digits and the sum of its digits.
#
#     Example 1:
#
# Input: n = 234
# Output: 15
# Explanation:
# Product of digits = 2 * 3 * 4 = 24
# Sum of digits = 2 + 3 + 4 = 9
# Result = 24 - 9 = 15
#
#     Example 2:
#
# Input: n = 4421
# Output: 21
# Explanation:
# Product of digits = 4 * 4 * 2 * 1 = 32
# Sum of digits = 4 + 4 + 2 + 1 = 11
# Result = 32 - 11 = 21


# @param {Integer} n
# @return {Integer}
def subtract_product_and_sum(n)
  arr = n.to_s.chars.map(&:to_i)
  product = arr.reduce(1, &:*)
  sum = arr.reduce(&:+)
  result = product - sum
  result
end

p subtract_product_and_sum(87492)