nums = [1, 0, 10, 1, 0, 0, 11, 20, 0, 1]

def move_zeroes(nums)
  count_zeros = nums.count(0)
  nums.delete(0)
  count_zeros.times do
    nums.push(0)
  end
  nums
end

p move_zeroes(nums)