# frozen_string_literal: true

coordinates = [[1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7]]
coordinates_two = [[1, 1], [2, 2], [3, 4], [4, 5], [5, 6], [7, 7]]
coordinates_three = [[1, 4], [2, 8], [3, 12], [4, 16], [5, 20], [6, 24]]
coordinates_four = [[-3, -2], [-1, -2], [2, -2], [-2, -2], [0, -2]]
coordinates_five = [[1, 2], [1, 2], [1, 2], [1, 2], [1, 2]]
coordinates_six = [[0, 1], [1, 3], [-4, -7], [5, 11]]
coordinate_false = [[-7,-3],[-7,-1],[-2,-2],[0,-8],[2,-2],[5,-6],[5,-5],[1,7]]

def check_straight_line(coordinates)
  i = 0
  x_array = []
  x_diff = []
  y_array = []
  y_diff = []
  equis = []
  ye = []
  coordinates.each do |point|
    x_array << point[0]
    y_array << point[1]
  end
  x_array.each_with_index do |elem, i|
    break if x_array[i + 1].nil?

    difference = x_array[i + 1] - elem
    x_diff << difference
  end
  y_array.each_with_index do |elem, i|
    break if y_array[i + 1].nil?

    difference = y_array[i + 1] - elem
    y_diff << difference
  end
  while i < x_diff.length && i < y_diff.length
    if y_diff[i] != 0
      equis << x_diff[i].to_f / y_diff[i]
    end
    if x_diff[i] != 0
      ye << y_diff[i].to_f / x_diff[i]
    end
    i += 1
  end
  x_diff.uniq!
  y_diff.uniq!
  equis.uniq!
  ye.uniq!
  if y_diff[0] == 0 && x_diff[0] == 0
    false
  elsif x_diff.length == 1 && y_diff.length == 1
    true
  elsif equis.length == 1 && ye.length == 1
    true
  elsif x_diff[0] == 0 && x_diff.length == 1
    true
  elsif y_diff[0] == 0 && y_diff.length == 1
    true
  else
    false
  end
end

p check_straight_line(coordinates)
puts
p check_straight_line(coordinates_two)
puts
p check_straight_line(coordinates_three)
puts
p check_straight_line(coordinates_four)
puts
p check_straight_line(coordinates_five)
puts
p check_straight_line(coordinates_six)
puts
p check_straight_line(coordinate_false)

