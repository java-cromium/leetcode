nums = [1,2,3,4,5]
k = 3

def rotate(nums, k)
  new_nums = []
  nums.each_with_index do |num, i|
    if i + k < nums.length
      new_nums[i+k] = num
    else
      new_nums[((nums.length - i) - k).abs] = num
    end
  end
  new_nums
end

rotate(nums, k)

# look into how to extract elements from an array backwards using a range (-1..x)

# the operation below is the same as a above but a little shorter using a ternary operator

def rotate_two(nums, k)
  new_nums = []
  nums.each_with_index do |num, i|
    i + k < nums.length ? new_nums[i+k] = num : new_nums[((nums.length - i) - k).abs] = num
  end
  return new_nums
end

p rotate_two(nums, k)