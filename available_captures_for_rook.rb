ary = [%w(. . . . . . . .),
       %w(. . . p . . . .),
       %w(. . . p . . . .),
       %w(p p . R . p B .),
       %w(. . . . . . . .),
       %w(. . . B . . . .),
       %w(. . . p . . . .),
       %w(. . . . . . . .)]

#
# num1 = diagonal1.inject { |prior_value, current_value| prior_value + current_value }
# num2 = diagonal2.inject { |prior_value, current_value| prior_value + current_value }
#

def multidimensional(arr)
  row = 0
  column = 0
# Loop over each row
  while (row < arr.size)
    puts "Row: " + row.to_s
    # Loop over each column
    while (column < arr[row].size)
      # Print the item at position row x column
      p arr[row][column]
      column = column + 1
      # p row
      # p column
    end
    # Reset column, advance row
    column = 0
    row = row + 1
  end
end

multidimensional(ary)

ary.each do |array|
  p array.include?('R')
end